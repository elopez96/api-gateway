FROM node:carbon

#1. DOWNLOAD CERTIFICATE APM
RUN apt-get update &&  apt-get install -y --no-install-recommends
#3. JAVA JDK
#RUN apt-get update &&  apt-get install -y --no-install-recommends default-jdk procps curl

#4. INSTALL LIBRERIA DE DEPENDENCIAS
RUN apt-get install -y alien libaio1 \
    -y apt-utils unzip telnet  \
    git build-essential python  

#6. CLEAN ARCHIVOS TEMPORALES
RUN apt-get clean  \ 
    && rm -rf /var/lib/apt/lists/* 

#7. CREA DIRECTORIOS WORK DIR
RUN mkdir -p /app/test
RUN mkdir -p /app/test/log
WORKDIR /app/test

COPY package.json /app/test/
RUN npm install

#9. COPYA ARCHIVOS A CARPETAS WORKDIR
COPY . /app/test
ENV NODE_ENV ${ENVIRONMENT}

#10. EXPONE SERVICIO PUERTO 3000
EXPOSE 3000
CMD [ "npm", "start" ]