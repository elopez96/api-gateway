const express= require('express');
const app=express();
const router = express.Router();

router.get('/',(req, res)=>{
    res.send('TEST 1 EXITOSO');
});

router.get('/test2',(req, res)=>{
    res.send('TEST 2 EXITOSO');
});


router.get('/test/test3',(req, res)=>{
    res.send('TEST 3 EXITOSO');
});

app.use('/', router);
app.listen(3000,"0.0.0.0");